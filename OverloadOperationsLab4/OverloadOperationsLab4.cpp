﻿#include "pch.h"
#include <iostream>
#include "MySettings.h"
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


using namespace std;

class myInteger {
#pragma warning(disable : 4996)
	char *IntegerStr;
	int count;

public:
	myInteger(int n = 100) 
	{
		count = 0;
		IntegerStr = new char[n];
	}

	~myInteger()
	{
		if (IntegerStr) delete IntegerStr;
	}

	friend istream & operator>>(istream &, myInteger &);
	friend ostream & operator<<(ostream &, myInteger &);

	void operator ++(int)
	{
		int i1;
		i1 = atoi(IntegerStr);
		i1++;
		sprintf(IntegerStr, "%u", i1);
	}

	void operator +=(const myInteger& c1)
	{
		int i1,i2;
		i1 = atoi(IntegerStr);
		i2 = atoi(c1.IntegerStr);
		i1+=i2;
		sprintf(IntegerStr, "%u", i1);
	}

	void operator +(const myInteger& c1) 
	{
		int i1, i2;
		i1 = atoi(IntegerStr); 
		i2 = atoi(c1.IntegerStr);
		i1 += i2;
		//cout << i1;


		sprintf(IntegerStr, "%u", i1);

		count = strlen(IntegerStr) + 1;
	}

	myInteger& operator =(const myInteger& c1)
	{
		if (this != &c1)
		{
			strcpy(IntegerStr, c1.IntegerStr);
			count = c1.count;
		}
		return *this;
	}

};

istream & operator>>(istream & input, myInteger &c1)
{
	int key;
	cout << "Введите целое число: ";
	do {
		key = _getch();
		if (key == '-' || (key >= '0' && key <= '9')) {
			c1.IntegerStr[c1.count++] = key;
			_putch(key);
		}
	} while (c1.count == 0 || key != 13);
	c1.IntegerStr[c1.count++] = '\x0';
	cout << endl;
	return input;
}

ostream & operator<<(ostream & output, myInteger &c1)
{
	int i;
	i = 0;
	cout << "число: ";
	while (i < c1.count)
	{
		_putch(c1.IntegerStr[i++]);
	}
	cout << endl;
	return output;
}

int main()
{
	MySettings mySettings;

	myInteger i;
	cin >> i;
	cout << "вывели строку " << i;
	i++;
	cout << "вывели результат операции ++ " << i;
	myInteger i1;
	i1 = i;
	cout << "вывели второе: " << i1;
	i1+i;
	cout << "результат сложения записали в второе " << i1;
	myInteger i2;
	cin >> i2;
	cout << "вывели третье: "<< i2;
	i2 += i1;
	cout << "результат сложения третьего и второго записали в третье " << i2;

	system("pause");
	return 0;

}


